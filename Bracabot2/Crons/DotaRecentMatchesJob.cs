﻿using AutoMapper;
using Bracabot2.Domain.Games.Dota2;
using Bracabot2.Domain.Interfaces;
using Bracabot2.Domain.Responses;
using Bracabot2.Domain.Support;
using Microsoft.Extensions.Options;
using Quartz;
using Serilog;

namespace Bracabot2.Crons
{
    [DisallowConcurrentExecution]
    public class DotaRecentMatchesJob : IRecentMatchesJob
    {
        private readonly ILogger logger;
        private readonly IDotaRepository dotaRepository;
        private readonly IDotaService dotaService;
        private readonly IMapper mapper;
        private readonly SettingsOptions config;

        public DotaRecentMatchesJob(IDotaRepository dotaRepository, IDotaService dotaService, IMapper mapper, IOptions<SettingsOptions> config)
        {
            this.logger = Log.ForContext<DotaRecentMatchesJob>();
            this.dotaRepository = dotaRepository;
            this.dotaService = dotaService;
            this.mapper = mapper;
            this.config = config.Value;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            logger.Debug("Initializing {0} cron job", nameof(DotaRecentMatchesJob));
            try
            {
                var dotaId = config.DotaId;

                var apiMatches = await dotaService.GetRecentMatchesAsync(dotaId);
                if (apiMatches == default)
                {
                    logger.Warning("A API do Dota não returnou resultados");
                    return;
                }

                var matchesCount = await dotaRepository.AddIfNotExistsAsync(apiMatches.Select(x => mapper.Map<Match>(x)));

                logger.Information("Number of matches inserted into database: {0}", matchesCount);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Failed to update de dota 2 games match history {0}", ex.Message);
            }
            logger.Debug("Finishing {0} cron job", nameof(DotaRecentMatchesJob));
        }
    }
}
