﻿using Bracabot2.Domain.Games.Dota2;
using Bracabot2.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Bracabot2.Repository
{
    public class DotaRepository : IDotaRepository
    {
        private readonly DotaContext context;

        public DotaRepository(DotaContext context)
        {
            this.context = context;
        }

        public async Task<Match> GetLastMatchAsync()
        {
            return await context.Matches.OrderByDescending(d => d.EndTime).FirstOrDefaultAsync();
        }

        public IEnumerable<Match> GetLastMatches(int limit, int skip = 0)
        {
            return context.Matches.OrderByDescending(d => d.EndTime).Skip(skip).Take(limit);
        }

        public IEnumerable<Match> GetLastMatches(DateTime fromEndTime, TimeSpan limit)
        {
            int i = 0;
            var lastMatch = context.Matches.OrderByDescending(d => d.EndTime).First();
            var currentMatch = lastMatch;
            while (currentMatch.EndTime >= fromEndTime 
                && currentMatch.EndTime - lastMatch.StartTime < limit)
            {
                yield return currentMatch;
                currentMatch = lastMatch;
                lastMatch = context.Matches.OrderByDescending(d => d.EndTime).Skip(++i).First();
            }
        }

        public async Task<IEnumerable<Match>> GetLastMatchesAsync(DateTime fromEndTime)
        {
            return await context.Matches.Where(d => d.EndTime >= fromEndTime).ToArrayAsync();
        }

        public Task<int> AddAsync(Match match)
        {
            context.Matches.Add(match);
            return context.SaveChangesAsync();
        }

        public async Task<int> AddIfNotExistsAsync(IEnumerable<Match> matches)
        {
            var ids = matches.Select(x => x.MatchId);
            var existingMatches = (await context.Matches.Where(x => ids.Contains(x.MatchId)).ToListAsync()).Select(x => x.MatchId).ToHashSet();

            matches.ToList().ForEach(match =>
            {
                if (!existingMatches.Contains(match.MatchId))
                {
                    context.Matches.Add(match);
                }
            });
            return await context.SaveChangesAsync();
        }
    }
}
