﻿using Bracabot2.Domain.Games.Dota2;
using Bracabot2.Domain.Interfaces;
using System.Text;

namespace Bracabot2.Commands
{
    public class ScoreCommand : ICommand
    {
        private readonly ITwitchService twitchService;        
        private readonly IDotaRepository dotaRepository;

        public ScoreCommand(ITwitchService twitchService, IDotaRepository dotaRepository)
        {
            this.twitchService = twitchService;            
            this.dotaRepository = dotaRepository;
        }

        public async Task<string> ExecuteAsync(string[] args)
        {
            var streamInfo = await twitchService.GetStreamInfo();
            if (streamInfo == default)
            {
                return "Streamer não está online";
            }

            if (!streamInfo.IsDota2Game)
            {
                return "Comando só disponível quando o streamer estiver jogando o jogo de Dota. !dota tem todas as informações.";
            }
            
            IEnumerable<Match> eligibleMatches = GetEligibleMatches();
            if (eligibleMatches == default)
            {
                return "Nenhuma partida encontrada. Seria essa a primeira do dia? ;)";
            }

            if (!eligibleMatches.Any())
            {
                return "Nenhuma partida encontrada. Seria essa a primeira do dia? ;)";
            }

            var statistics = new Dota2Statistics(eligibleMatches);
            if (statistics.HasError)
            {
                return statistics.ErrorDescription;
            }

            var sb = new StringBuilder();
            sb.Append($"TOTAL (J = {statistics.Games} --- V -> {(statistics.Victories != 0 ? statistics.Victories.ToString() : "Nenhuma")} --- D -> {(statistics.Defeats != 0 ? statistics.Defeats.ToString() : "Nenhuma")})");
            if (statistics.RankedGames == 0)
            {
                sb.Append(" ---- Nenhuma foi RANKED");                
            }
            else if (statistics.Games == statistics.RankedGames)
            {
                sb.Append(" ---- Todas foram RANKED");
            }
            else
            {
                sb.Append($" --- RANKED (J = {statistics.RankedGames} --- V -> {(statistics.RankedVictories != 0 ? statistics.RankedVictories.ToString() : "Nenhuma")} --- D -> {(statistics.RankedDefeats != 0 ? statistics.RankedDefeats.ToString() : "Nenhuma")})");
            }

            return sb.ToString();            
        }

        public bool IsEligible(DateTime currentMatch, DateTime? lastMatch)
        {
            var limit = TimeSpan.FromHours(5);                

            TimeSpan diffMatches = lastMatch == default 
                ? DateTime.UtcNow - currentMatch 
                : lastMatch.Value - currentMatch;

            return diffMatches < limit;
        }

        public IEnumerable<Match> GetEligibleMatches()
        {
            var eligibleMatches = new List<Match>();
            int i = 0;

            Match lastMatch = null;
            bool retrieveMore;
            do
            {
                var matches = dotaRepository.GetLastMatches(10, 10 * i);
                i++;

                if (matches == null)
                    return eligibleMatches;

                retrieveMore = matches.Any();
                foreach (var currentMatch in matches)
                {
                    if (!IsEligible(currentMatch.EndTime, lastMatch?.EndTime))
                    {
                        retrieveMore = false;
                        break;
                    }
                    eligibleMatches.Add(currentMatch);
                    lastMatch = currentMatch;
                }
            } 
            while(retrieveMore);            

            return eligibleMatches;
        }
    }
}
